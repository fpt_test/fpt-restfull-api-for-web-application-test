﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FPT_API.DataAccessLayer;
using FPT_API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FPT_API.Controllers
{
    public class ArtistsController : ApiController
    {
        private AccessDB db = new AccessDB();
        private static HttpClient client = new HttpClient();
        private static string youtubeURL = "https://www.youtube.com/embed/";
        private static string youtubeAPI = "https://www.googleapis.com/youtube/v3/search?";
        private static string youtubeToken = "AIzaSyBvivThFPC0iroucwIb5J_5YVhbxORy8RM";

        // GET: api/Artists
        public IQueryable<Artist> Getartist()
        {
            return db.artist;
        }

        // GET: api/Artists/5
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> GetArtist(int id)
        {
            Artist artist = await db.artist.FindAsync(id);
            if (artist == null)
            {
                return NotFound();
            }

            return Ok(artist);
        }

        // PUT: api/Artists/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtist(int id, Artist artist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artist.ArtistID)
            {
                return BadRequest();
            }

            db.Entry(artist).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artists
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> PostArtist(Artist artist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.artist.Add(artist);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = artist.ArtistID }, artist);
        }

        // DELETE: api/Artists/5
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> DeleteArtist(int id)
        {
            Artist artist = await db.artist.FindAsync(id);
            if (artist == null)
            {
                return NotFound();
            }

            db.artist.Remove(artist);
            await db.SaveChangesAsync();

            return Ok(artist);
        }

        [HttpPost]
        [Route("api/youtube")]
        [ResponseType(typeof(Artist))]
        public async Task<IHttpActionResult> GetYoutubeSearch(string keySearch, string tokenPage = null)
        {
            string pageToken = tokenPage != null ? "&pageToken=" + tokenPage : string.Empty;
            string Url = string.Format(youtubeAPI + "part=id,snippet{0}&q={1}&type=video&key={2}", pageToken, keySearch, youtubeToken);
            string response = await CallAPI(Url);
            JObject Data = JsonConvert.DeserializeObject<JObject>(response);
            List<YoutubeResult> listSearch = new List<YoutubeResult>();

            if (Data != null)
            {
                try
                {
                    for (int i = 0; i < ((JArray) Data["items"]).Count; i++)
                    {
                        YoutubeResult item = new YoutubeResult();
                        item.ImageURL = ((JArray) Data["items"])[i]["snippet"]["thumbnails"]["medium"]["url"]
                            .ToString();
                        item.ReleaseDate =
                            Convert.ToDateTime(((JArray) Data["items"])[i]["snippet"]["publishedAt"].ToString());
                        item.artistName = ((JArray) Data["items"])[i]["snippet"]["channelTitle"].ToString();
                        item.nextPageToken = Data["nextPageToken"] == null ? string.Empty : Data["nextPageToken"].ToString();
                        item.previousPageToken = Data["prevPageToken"] == null
                            ? string.Empty
                            : Data["prevPageToken"].ToString();
                        item.title = ((JArray) Data["items"])[i]["snippet"]["title"].ToString();
                        item.totalResult = Convert.ToInt32(Data["pageInfo"]["totalResults"].ToString());
                        item.videoId = ((JArray) Data["items"])[i]["id"]["videoId"].ToString();

                        listSearch.Add(item);
                    }

                    Data = null;
                }
                catch (Exception e)
                {
                    Data = (JObject)e.Message;
                }
            }

            if(Data != null)
                return Json(new { @data = listSearch, @error = Data });

            return Json(listSearch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtistExists(int id)
        {
            return db.artist.Count(e => e.ArtistID == id) > 0;
        }

        private static async Task<string> CallAPI(string url)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response = client.GetAsync(url).Result;

            string getResponse = await response.Content.ReadAsStringAsync();

            return getResponse;
        }
    }

    public class YoutubeResult
    {
        public string nextPageToken { get; set; }
        public string previousPageToken { get; set; }
        public string videoId { get; set; }
        public int totalResult { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string title { get; set; }
        public string ImageURL { get; set; }
        public string artistName { get; set; }
    }
}