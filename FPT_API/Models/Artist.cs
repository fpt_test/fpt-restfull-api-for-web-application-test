﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FPT_API.Models
{
    [Table("Artist", Schema = "dbo")]
    public class Artist
    {
        [Key]
        public int ArtistID { get; set; }
        public string ArtistName {get; set; }
        public string AlbumName { get; set; }
        public string ImageURL { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public decimal Price { get; set; }
        public string SampleURL { get; set; }
    }
}