﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using FPT_API.Models;

namespace FPT_API.DataAccessLayer
{
    public class AccessDB : DbContext
    {
        public AccessDB() : base("FPT_TESTDB")
        {
        }

        public DbSet<Artist> artist { get; set; }
    }
}